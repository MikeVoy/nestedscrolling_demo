package customview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.NestedScrollingParent;
import android.support.v4.view.NestedScrollingParentHelper;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.stx.xhb.xbanner.XBanner;

public class MyNestedScrollParent extends LinearLayout implements NestedScrollingParent {
    private NestedScrollingParentHelper mParentHelper;
    private XBanner banner;
    private LinearLayout linearLayout;
    private MyNestedScrollChild nsc;
    private int bannerHeight;
    private int layoutHeight;
    public MyNestedScrollParent(Context context) {
        super(context);
        init();
    }

    public MyNestedScrollParent(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mParentHelper = new NestedScrollingParentHelper(this);
    }

    //获取子View


    @Override
    protected void onFinishInflate() {

        banner = (XBanner) getChildAt(0);
        linearLayout = (LinearLayout) getChildAt(1);
        nsc = (MyNestedScrollChild) getChildAt(2);


        banner.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (bannerHeight <= 0) {
                    bannerHeight = banner.getMeasuredHeight();
                }
            }
        });
        linearLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (layoutHeight <= 0) {
                    layoutHeight = linearLayout.getMeasuredHeight();
                }
            }
        });
        super.onFinishInflate();
    }

    @Override
    public boolean onStartNestedScroll(View child, View target, int nestedScrollAxes) {
       /*nestedScrollAxes，嵌套滑动的坐标系，也就是用来判断X轴滑动还是Y轴滑动，
       这里可以根据需要返回true和false
       ，当然了如果返回false，嵌套滑动就没得玩了，*/
        if (target instanceof MyNestedScrollChild) {
            return true;
        }
        return true;
    }

    @Override
    public void onStopNestedScroll(View child) {
        mParentHelper.onStopNestedScroll(child);
    }

    @Override
    public void onNestedScrollAccepted(View child, View target, int axes) {
       mParentHelper.onNestedScrollAccepted(child, target, axes);
    }
    //先于child滚动
    //前3个为输入参数，最后一个是输出参数


    @Override
    public void onNestedPreScroll(View target, int dx, int dy, int[] consumed) {
        if (showImg(dy) || hideImg(dy)) {//如果需要显示或隐藏图片，即需要自己(parent)滚动
            scrollBy(0, -dy);//滚动
            consumed[1] = dy;//告诉child我消费了多少
        }
    }

    //后于child滚动
    @Override
    public void onNestedScroll(View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {

    }
    //返回值：是否消费了fling
    @Override
    public boolean onNestedPreFling(View target, float velocityX, float velocityY) {
        return false;
    }

    //返回值：是否消费了fling
    @Override
    public boolean onNestedFling(View target, float velocityX, float velocityY, boolean consumed) {
        return false;
    }

    @Override
    public int getNestedScrollAxes() {
        return mParentHelper.getNestedScrollAxes();
    }
    //下拉的时候是否要向下滚动以显示图片
    public boolean showImg(int dy) {
        if (dy > 0) {
            if (getScrollY() > 0 && nsc.getScrollY() == 0) {
                return true;
            }
        }
        return false;
    }
    //上拉的时候，是否要向上滚动，隐藏图片
    public boolean hideImg(int dy) {
        if (dy < 0) {
            if (getScrollY() < bannerHeight) {
                return true;
            }
        }
        return false;
    }
    //scrollBy内部会调用scrollTo
    //限制滚动范围
    @Override
    public void scrollTo(int x, int y) {
        if (y < 0) {
            y = 0;
        }
        if (y > bannerHeight) {
            y = bannerHeight;
        }

        super.scrollTo(x, y);
    }

}
