package com.example.nestedscrolling_demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.stx.xhb.xbanner.XBanner;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements XBanner.XBannerAdapter {

    private List<String> imageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        XBanner banner = findViewById(R.id.mBanner);
         imageUrl = new ArrayList<>();

         imageUrl.add("http://pic1.cxtuku.com/00/06/78/b9903ad9ea2b.jpg");
        imageUrl.add("http://pic1.cxtuku.com/00/06/78/b9903ad9ea2b.jpg");
        imageUrl.add("http://pic1.cxtuku.com/00/06/78/b9903ad9ea2b.jpg");
        imageUrl.add("http://pic1.cxtuku.com/00/06/78/b9903ad9ea2b.jpg");

        banner.setData(imageUrl,null);
        banner.setPoinstPosition(XBanner.RIGHT);
        banner.setmAdapter(this);
    }

    @Override
    public void loadBanner(XBanner banner, Object model, View view, int position) {
        Glide.with(this).load(imageUrl.get(position)).into((ImageView)view);
    }


}
