package com.example.nestedscrolling_demo.app;

import android.app.Application;

import me.jessyan.autosize.AutoSizeConfig;
import me.jessyan.autosize.DefaultAutoAdaptStrategy;

public class Myapp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AutoSizeConfig.getInstance()
                .setCustomFragment(false)
                .setLog(true)
                .setUseDeviceSize(true)
                .setBaseOnWidth(false)
                .setAutoAdaptStrategy(new DefaultAutoAdaptStrategy());


    }
}
